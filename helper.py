import json
import logging
import logging.config
import os


def setup_config(path: str) -> dict:
    """
    Setup the config object for all configuration settings required by the app
    :param path: path of config file
    :return: return the config object
    """
    try:
        with open(path, 'r') as fp:
            config = json.load(fp)
    except (AttributeError, IOError, ImportError, TypeError, ValueError, json.JSONDecodeError) as e:
        raise e
    else:
        return config


def setup_logging(name: str, config: dict) -> logging.Logger:
    """
    Setup the logger with config settings
    :param name: name of the logger
    :param config: config object for the application
    :return:
    """
    log_dir = config.get('LOG').get('DIR_PATH')
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    try:
        with open(config.get('LOG').get('CONFIG_PATH'), 'rt') as fp:
            logging.config.dictConfig(json.load(fp))
    except (AttributeError, IOError, ImportError, TypeError, ValueError, json.JSONDecodeError):
        logging.basicConfig(level=config.get('LOG').get('DEFAULT_LEVEL'))

    return logging.getLogger(name)
