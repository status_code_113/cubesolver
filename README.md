cubesolver
==========

### Intro
This package tries to attempt a automated solving of a rubik's cube given the algorithm as input.

### Dependencies
We have used `pip` as the dependency management solution. Dependencies can be installed using `requirements.txt` 
file.

* `Click==7.0`
* `pycuber==0.2.2`

### Contact
* [ssvasista](mailto:ssvasista@gmail.com)
* [onlinejudge95](mailto:onlinejudge95@gmail.com)
