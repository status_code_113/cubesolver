import click

import cubesolver
import helper


@click.command()
@click.argument('algorithm')
@click.argument('config_file_path')
def cli(algorithm: str, config_file_path: str):
    """
    CLI Entry point for the app.
    :param algorithm: algorithm to be run on the cube
    :param config_file_path: path of the config file for the app
    :return: None
    """
    config = helper.setup_config(config_file_path)
    logger = helper.setup_logging(__name__, config)
    logger.info('Initialised config and logs')

    try:
        solved, random = cubesolver.get_cube()
        logger.info(f'Solved cube state\n{solved}')
        logger.info(f'Random state is\n{random}')

        algorithm = algorithm.split(',')
        logger.info(f'Applying algorithm {algorithm}')

        for index, step in enumerate(algorithm):
            random(step)
            logger.info(f'Iteration:{index}\tStep:{step}')
            logger.info(f'State\n{random}')
    except Exception as e:
        logger.exception(e)


if __name__ == '__main__':
    cli()
