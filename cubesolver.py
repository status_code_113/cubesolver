import pycuber as pc


VERSION = "0.1.0"


def get_cube() -> (pc.Cube, pc.Cube):
    try:
        return pc.Cube(), pc.Cube()(pc.Formula().random())
    except Exception as e:
        raise e
